import { setActiveUserName } from '../../src/store/sagas/user'
import uuidv4 = require('uuid/v4')
import { put, call } from '@redux-saga/core/effects'
import { requestSetUserName, setActiveOnboarding } from '../../src/store/reducer/actions/general'
import { OnboardingRemoteApi } from '../../src/store/api/remote/onboarding'
import { SubscriptionApi } from '../../src/store/api/subscribers'
import {
  commitOnboarding,
  requestStartOnboarding,
} from '../../src/store/reducer/actions/onboarding'

const defaultOnboarding = {
  _id: uuidv4(),
  userName: uuidv4(),
  startedAt: new Date(),
  finishedAt: null,
  step: {
    current: Math.floor(Math.random() * 10),
    total: Math.floor(Math.random() * 10),
  },
  imageUrl: uuidv4(),
}

describe(`Saga 'setActiveUserName'`, () => {
  let mockedId: string = uuidv4()
  let gen: ReturnType<typeof setActiveUserName>

  beforeEach(() => {
    mockedId = uuidv4()
    gen = setActiveUserName(requestSetUserName(mockedId))
  })

  it(`when userName is null - sets activeOnboarding as null and finishes execution`, () => {
    gen = setActiveUserName(requestSetUserName(null))
    expect(gen.next().value).toEqual(put(setActiveOnboarding(null)))
    expect(gen.next().done).toBeTruthy()
  })

  it(`attempts to refresh socket subscriptions with new userName and fetch activeOnboarding`, () => {
    expect(gen.next().value).toEqual(call(SubscriptionApi.refreshSubscriptionRoom, mockedId))
    expect(gen.next().value).toEqual(call(OnboardingRemoteApi.getActiveOnboarding, mockedId))
  })

  it(`when fetch is successful - commits onboarding, sets it as active and finishes execution`, () => {
    gen.next()
    gen.next()
    expect(gen.next(defaultOnboarding).value).toEqual(put(commitOnboarding(defaultOnboarding)))
    expect(gen.next().value).toEqual(put(setActiveOnboarding(defaultOnboarding._id)))
    expect(gen.next().done).toBeTruthy()
  })

  it(`when fetch is unsuccessful - starts new onboarding and finishes execution`, () => {
    gen.next()
    gen.next()

    expect(gen.next(null).value).toEqual(put(requestStartOnboarding()))
    expect(gen.next().done).toBeTruthy()
  })
})
