import {
  createOnboarding,
  finishOnboarding,
  nextStepOfOnboarding,
} from '../../src/store/sagas/onboarding'
import {
  requestFinishOnboarding,
  requestNextStepOfOnboarding,
  requestStartOnboarding,
  rollbackOnboarding,
} from '../../src/store/reducer/actions/onboarding'
import { select, put, call, delay } from '@redux-saga/core/effects'
import { getActiveUserName } from '../../src/store/reducer/selectors/general'
import uuidv4 = require('uuid/v4')
import { setActiveOnboarding } from '../../src/store/reducer/actions/general'
import { OnboardingRemoteApi } from '../../src/store/api/remote/onboarding'

const defaultOnboarding = {
  _id: uuidv4(),
  userName: uuidv4(),
  startedAt: new Date(),
  finishedAt: null,
  step: {
    current: Math.floor(Math.random() * 10),
    total: Math.floor(Math.random() * 10),
  },
  imageUrl: uuidv4(),
}

describe(`Saga 'createOnboarding'`, () => {
  let mockedId: string = uuidv4()
  let gen: ReturnType<typeof createOnboarding>

  beforeEach(() => {
    mockedId = uuidv4()
    gen = createOnboarding(requestStartOnboarding())
  })

  it(`asserts if activeUserName is defined`, () => {
    expect(gen.next().value).toEqual(select(getActiveUserName))
    expect(gen.next(null).done).toBeTruthy()
  })

  it('sets active onboarding to null before fetching data', () => {
    gen.next()
    expect(gen.next(mockedId).value).toEqual(put(setActiveOnboarding(null)))
  })

  it('attempts to create onboarding for active user', () => {
    gen.next()
    gen.next(mockedId)

    expect(gen.next().value).toEqual(call(OnboardingRemoteApi.createOnboarding, mockedId))
  })

  it('when fetch is successful - puts activeOnboardingId and finishes execution', () => {
    gen.next()
    gen.next(mockedId)
    gen.next()

    expect(gen.next(defaultOnboarding._id).value).toEqual(
      put(setActiveOnboarding(defaultOnboarding._id))
    )
    expect(gen.next().done).toBeTruthy()
  })

  it('when fetch is unsuccessful - makes backoff, requests start again and finishes execution', () => {
    gen.next()
    gen.next(mockedId)
    gen.next()

    expect(gen.next(null).value).toEqual(delay(500))
    expect(gen.next().value).toEqual(put(requestStartOnboarding()))
    expect(gen.next().done).toBeTruthy()
  })
})

describe(`Saga 'finishOnboarding'`, () => {
  let mockedId: string = uuidv4()
  let date: Date = new Date()
  let gen: ReturnType<typeof finishOnboarding>

  beforeEach(() => {
    mockedId = uuidv4()
    const action = requestFinishOnboarding(mockedId)

    date = action.payload.finishedAt!
    gen = finishOnboarding(action)
  })

  it(`attempts to update selected onboarding`, () => {
    expect(gen.next().value).toEqual(
      call(OnboardingRemoteApi.updateOnboarding, mockedId, {
        _id: mockedId,
        finishedAt: date,
      })
    )
  })

  it(`when update is successful - finishes execution`, () => {
    gen.next()
    expect(gen.next(defaultOnboarding).done).toBeTruthy()
  })

  it(`when update is unsuccessful - rollbacks onboarding and finishes execution`, () => {
    gen.next()
    expect(gen.next(null).value).toEqual(put(rollbackOnboarding(mockedId)))
    expect(gen.next().done).toBeTruthy()
  })
})

describe(`Saga 'nextStepOfOnboarding'`, () => {
  let mockedId: string = uuidv4()
  let step = { current: Math.floor(Math.random() * 10) }
  let gen: ReturnType<typeof nextStepOfOnboarding>

  beforeEach(() => {
    mockedId = uuidv4()
    const action = requestNextStepOfOnboarding(mockedId, Math.floor(Math.random() * 10))
    step = {
      current: action.payload.step!.current!,
    }
    gen = nextStepOfOnboarding(action)
  })

  it(`attempts to update selected onboarding`, () => {
    expect(gen.next().value).toEqual(
      call(OnboardingRemoteApi.updateOnboarding, mockedId, {
        _id: mockedId,
        step: step,
      })
    )
  })

  it(`when update is successful - finishes execution`, () => {
    gen.next()
    expect(gen.next(defaultOnboarding).done).toBeTruthy()
  })

  it(`when update is unsuccessful - rollback onboarding and finishes execution`, () => {
    gen.next()
    expect(gen.next(null).value).toEqual(put(rollbackOnboarding(mockedId)))
    expect(gen.next().done).toBeTruthy()
  })
})
