import {
  ACTION_ONBOARDING,
  commitOnboarding,
  requestFinishOnboarding,
  requestNextStepOfOnboarding,
  requestStartOnboarding,
  rollbackOnboarding,
} from '../../src/store/reducer/actions/onboarding'
import uuidv4 = require('uuid/v4')

describe(`Action 'requestStartOnboarding'`, () => {
  it(`Returns REQUEST.START action type`, () => {
    const action = requestStartOnboarding()

    expect(action.type).toBe(ACTION_ONBOARDING.REQUEST.START)
  })
})

describe(`Action 'requestFinishOnboarding'`, () => {
  it(`Returns REQUEST.FINISH action type`, () => {
    const action = requestFinishOnboarding('')
    expect(action.type).toBe(ACTION_ONBOARDING.REQUEST.FINISH)
  })

  it(`Returns correct payload`, () => {
    const mockedId = uuidv4()
    const action = requestFinishOnboarding(mockedId)
    const { _id, finishedAt } = action.payload

    expect(_id).toBe(mockedId)
    expect(finishedAt).toBeDefined()
    expect(finishedAt).not.toBeNull()
  })
})

describe(`Action 'requestNextStepOfOnboarding'`, () => {
  it(`Returns REQUEST.NEXT_STEP`, () => {
    const action = requestNextStepOfOnboarding('', 0)
    expect(action.type).toBe(ACTION_ONBOARDING.REQUEST.NEXT_STEP)
  })

  it(`Returns correct payload (with step.current++)`, () => {
    const mockedId = uuidv4()
    const randomStep = Math.floor(Math.random() * 10)
    const action = requestNextStepOfOnboarding(mockedId, randomStep)

    const { _id, step } = action.payload
    expect(_id).toBe(mockedId)
    expect(step).toBeDefined()
    expect(step!.current).toBe(randomStep + 1)
  })
})

describe(`Action 'rollbackOnboarding'`, () => {
  it(`Returns ROLLBACK`, () => {
    const action = rollbackOnboarding('')
    expect(action.type).toBe(ACTION_ONBOARDING.ROLLBACK)
  })

  it(`Returns correct payload`, () => {
    const mockedId = uuidv4()

    const action = rollbackOnboarding(mockedId)
    const { _id } = action.payload

    expect(_id).toBe(mockedId)
  })
})

describe(`Action 'commitOnboarding'`, () => {
  const defaultOnboarding = {
    _id: uuidv4(),
    userName: uuidv4(),
    startedAt: new Date(),
    finishedAt: null,
    step: {
      current: Math.floor(Math.random() * 10),
      total: Math.floor(Math.random() * 10),
    },
    imageUrl: uuidv4(),
  }
  it(`Returns COMMIT`, () => {
    const action = commitOnboarding(defaultOnboarding)
    expect(action.type).toBe(ACTION_ONBOARDING.COMMIT)
  })

  it(`Returns exact onboarding in payload`, () => {
    const action = commitOnboarding(defaultOnboarding)
    expect(action.payload).toMatchObject(defaultOnboarding)
  })
})
