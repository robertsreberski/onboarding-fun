import {
  ACTION_GENERAL,
  requestSetUserName,
  setActiveOnboarding,
} from '../../src/store/reducer/actions/general'
import uuidv4 = require('uuid/v4')

describe(`Action 'requestSetUserName'`, () => {
  it(`Returns CHANGE_VALUE action type`, () => {
    const action = requestSetUserName('')

    expect(action.type).toBe(ACTION_GENERAL.CHANGE_VALUE)
  })

  it(`Returns unchanged userName`, () => {
    const mockedUserName = uuidv4()
    const action = requestSetUserName(mockedUserName)

    const { activeUserName } = action.payload
    expect(activeUserName).toBe(mockedUserName)
  })
})

describe(`Action 'setActiveOnboarding'`, function() {
  it(`Returns CHANGE_VALUE action type`, () => {
    const action = setActiveOnboarding('')

    expect(action.type).toBe(ACTION_GENERAL.CHANGE_VALUE)
  })

  it(`Returns unchanged onboardingId`, () => {
    const mockedId = uuidv4()
    const action = setActiveOnboarding(mockedId)

    const { activeOnboardingId } = action.payload
    expect(activeOnboardingId).toBe(mockedId)
  })
})
