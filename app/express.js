const express = require("express");
const compression = require("compression")

const app = express();

const PORT_NUMBER = 8000;
const SOURCE_DIR= 'dist';

app.use(compression());
app.use(express.static(SOURCE_DIR));

app.listen(PORT_NUMBER, () => {
  console.log(`Express web server started: http://${process.env.SERVER_IP}:${PORT_NUMBER}`);
  console.log(`Serving content from /${SOURCE_DIR}`);
});
