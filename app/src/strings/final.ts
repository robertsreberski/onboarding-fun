interface IFinalStrings {
  title: string
  subtitle: string
}

const FinalStrings: IFinalStrings = {
  title: `You've reached the end of onboarding`,
  subtitle: `In case you like it there is an option for you to experience it again.`,
}

export default FinalStrings
