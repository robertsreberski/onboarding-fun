interface INameStrings {
  title: string
  subtitle: string
}

const NameStrings: INameStrings = {
  title: `What's your name?`,
  subtitle: `I will remember you, I promise!`,
}

export default NameStrings
