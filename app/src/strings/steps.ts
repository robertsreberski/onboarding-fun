interface IStepStrings {
  title: string
  subtitle: string
  list: {
    header: string
    items: {
      title: string
      text: string
    }[]
  }
}

interface IStepsStrings {
  [step: string]: IStepStrings
  default: IStepStrings
  1: IStepStrings
  2: IStepStrings
  3: IStepStrings
}

const StepsStrings: IStepsStrings = {
  default: { 
    title: '',
    subtitle: '',
    list: {
      header: '',
      items: []
    }
  },
  1: {
    title: 'Hi there!',
    subtitle:
      'Welcome to the onboarding. Instead of just showcasing the app I will tell you something about it in the meantime.',
    list: {
      header: 'About the Frontend App',
      items: [
        {
          title: 'Components',
          text: `React, React Hooks, styled-components`,
        },
        {
          title: 'State',
          text: `Redux, Redux-Saga (optimistic updates with rollbacks), Reselect`,
        },
        {
          title: 'Communication',
          text: `Axios, Socket.IO`,
        },
      ],
    },
  },
  2: {
    title: 'Synced sessions',
    subtitle:
      'Why not! I encourage you to open new browser window and check out how state changes reflect between sessions.',
    list: {
      header: 'About the Backend',
      items: [
        {
          title: 'Database',
          text: `MongoDB, Mongoose`,
        },
        {
          title: 'REST Api',
          text: `Express.JS`,
        },
        {
          title: 'Subscriptions',
          text: `Socket.IO`,
        },
      ],
    },
  },
  3: {
    title: 'Background',
    subtitle: `It is fetched as a random image from Unsplashed and is perserved for single onboarding session.`,
    list: {
      header: 'About the deploy',
      items: [
        {
          title: 'Dockerized',
          text: `Dockerfiles, docker-compose`,
        },
        {
          title: 'Security',
          text: `Only port 80 is open for public`,
        },
      ],
    },
  },
}

export default StepsStrings
