import * as React from 'react'
import { connect } from 'react-redux'
import Onboarding from '../screens/Onboarding'
import Name from '../screens/Name'

type StateProps = {
  hasUserName: boolean
}

type DispatchProps = {}

type OwnProps = {}

type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps: (state: AppState) => StateProps = state => ({
  hasUserName: state.General.activeUserName !== null,
})

const SimpleRouter: React.FC<Props> = ({ hasUserName }) => {
  if (hasUserName) return <Onboarding />
  else return <Name />
}

export default connect(mapStateToProps)(SimpleRouter)
