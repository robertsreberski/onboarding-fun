import styled, { css, SimpleInterpolation, CSSObject } from 'styled-components'
import Theme from '../theme'

export const FullCenter = styled.div`
  position: relative;
  display: flex;

  width: 100%;
  height: 100%;

  flex-direction: column;
  justify-content: center;
  align-items: center;

  background-color: ${Theme.color.background.white};
`

export const BackgroundImage = styled.div<{ src?: string; darker?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  background-image: ${({ src }) => (src ? `url("${src}")` : `none`)};

  background-color: ${Theme.color.background.black};
  background-size: cover;

  filter: brightness(${({ darker }) => (darker ? '50%' : '80%')});

  transition: filter 1s;
`

export const BackgroundColor = styled.div<{ color?: string }>`
  position: absolute;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  background-color: ${({ color }) => color || Theme.color.background.white};
`

const breakpoints: { [key: string]: number } = {
  xl: 1680,
  l: 1200,
  m: 992,
  s: 768,
}

export type MediaQuery = { [key in keyof typeof breakpoints]: typeof css }

const mediaQuery = () =>
  Object.keys(breakpoints).reduce(
    (acc, label) => {
      acc[label] = ((
        first: TemplateStringsArray | CSSObject,
        ...interpolations: SimpleInterpolation[]
      ) =>
        css`
          @media (min-width: ${breakpoints[label]}px) {
            ${css(first, ...interpolations)};
          }
        `) as typeof css
      return acc
    },
    {} as MediaQuery
  )

export const media: MediaQuery = mediaQuery()
