import styled, { css } from 'styled-components'
import Theme from '../theme'
import { growAnimation } from './effects'
import { rgba } from 'polished'

export const ContainedButton = styled.button<{ disabled?: boolean; background?: string }>`
  background-color: ${({ background }) => background || Theme.color.accent};
  color: ${Theme.color.text.white};
  font-size: 1.5em;
  font-weight: 700;

  min-width: 15rem;
  text-align: center;
  text-transform: uppercase;

  border-radius: 8px;
  padding: ${Theme.space.lg} ${Theme.space.xxxl};
  margin: 0 ${Theme.space.md};

  cursor: pointer;
  outline: none;
  border: none;

  box-shadow: ${Theme.color.shadow.normal};

  ${({ disabled }) => !disabled && growAnimation};
  ${({ disabled }) =>
    disabled &&
    css`
      cursor: default;
      opacity: 0.5;
    `};
`

export const SimpleInput = styled.input`
  font-size: 1.5em;

  padding: ${Theme.space.lg} ${Theme.space.xxl};

  border: none;
  border-radius: 8px;

  background-color: ${Theme.color.background.grey};
  width: 25rem;
`
