import styled from 'styled-components'
import Theme from '../theme'
import { rgba } from 'polished'
import { media } from './common'

export const Title = styled.h2<{ color?: string }>`
  color: ${({ color }) => color || Theme.color.text.black};
  font-size: 6vw;
  font-weight: 700;

  margin: 0 0 ${Theme.space.xl};

  @media screen and (min-width: 1600px) {
    font-size: 5.75vw;
  }
`

export const SubTitle = styled.p<{ color?: string }>`
  color: ${({ color }) => color || Theme.color.text.black};
  font-size: 2.25vw;
  font-weight: 200;

  margin: 0 0 ${Theme.space.lg};

  ${media.xl`
    font-size: 2vw;
  `}
`

export const AccentCaption = styled.span<{ color?: string }>`
  color: ${({ color }) => color || Theme.color.accent};
  font-size: 1vw;
  font-weight: 700;

  ${media.xl`
    font-size: 0.75vw;
  `}
`

export const ListHeader = styled.h4<{ color?: string }>`
  color: ${({ color }) => color || rgba(Theme.color.text.black, 0.8)};
  font-size: 2vw;
  font-weight: 700;

  margin: 0 0 ${Theme.space.xl};

  ${media.xl`
    font-size: 1.75vw;
  `}
`

export const ListItemText = styled.p<{ color?: string }>`
  color: ${({ color }) => color || Theme.color.text.black};
  margin: 0;
  font-size: 1.25vw;
  font-weight: 500;

  ${media.xl`
    font-size: 1vw;
  `}
`

export const Iconic = styled.i<{ color?: string }>`
  color: ${({ color }) => color || Theme.color.text.black};
  width: 32px;
  height: 32px;
  font-size: 32px;
`
