import { css } from 'styled-components'

export const pulsate = css``

export const growAnimation = css`
  transition: all 0.2s ease-in-out;
  &:hover {
    box-shadow: 0 4px 8px 2px rgba(0, 0, 0, 0.33);
    transform: scale(1.05);
  }
`
