import { createGlobalStyle } from 'styled-components'
import Theme from '../theme'

export const GlobalStyle = createGlobalStyle`

  body {
    font-family: 'Montserrat', 'Arial', sans-serif;
    background-color: ${Theme.color.background.white};
    margin: 0;
  }

  #root {
    overflow: hidden;
    width: 100%;
    height: 100vh;
  }
`
