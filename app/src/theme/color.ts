export interface IColor {
  accent: string
  text: {
    white: string
    black: string
  }
  background: {
    white: string
    black: string
    grey: string
  }
  shadow: {
    normal: string
  }
}

export default {
  accent: '#db3569',
  text: {
    white: '#fafafa',
    black: '#212121',
  },
  background: {
    white: '#fff',
    grey: '#ebebeb',
    black: '#212121',
  },
  shadow: {
    normal: '-2px 0px 8px rgba(0, 0, 0, 0.2)',
  },
} as IColor
