import * as React from 'react'
import { FinalActions, FinalBody, FinalHeader } from './styles'
import { ContainedButton } from '../../styles/inputs'
import { SubTitle, Title } from '../../styles/texts'
import Theme from '../../theme'
import FinalStrings from '../../strings/final'

export interface Props {
  onRestart: () => void
  onLogout: () => void
}

const Final: React.FC<Props> = ({ onRestart, onLogout }) => {
  return (
    <FinalBody>
      <FinalHeader>
        <Title color={Theme.color.text.white}>{FinalStrings.title}</Title>
        <SubTitle color={Theme.color.text.white}>{FinalStrings.subtitle}</SubTitle>
      </FinalHeader>
      <FinalActions>
        <ContainedButton onClick={onRestart}>Restart</ContainedButton>
        <ContainedButton onClick={onLogout}>Change Name</ContainedButton>
      </FinalActions>
    </FinalBody>
  )
}

export default Final
