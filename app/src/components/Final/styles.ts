import styled from 'styled-components'
import Theme from '../../theme'

export const FinalBody = styled.div`
  position: relative;
  display: flex;

  height: 100%;

  margin: 0 ${Theme.space.xxxl};

  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const FinalHeader = styled.div`
  text-align: center;
  margin-bottom: ${Theme.space.xxxl};
`

export const FinalActions = styled.div`
  position: relative;
  display: flex;

  align-items: center;
`
