import styled from 'styled-components'
import Theme from '../../theme'
import { media } from '../../styles/common'

export const StepContainer = styled.div`
  position: relative;
  display: flex;

  height: 100%;
  width: 100%;

  flex-direction: row;
  justify-content: flex-end;
`

export const StepBody = styled.div`
  position: relative;
  display: flex;

  width: 65%;

  flex-direction: column;

  padding: ${Theme.space.lg} ${Theme.space.xxl};

  box-shadow: ${Theme.color.shadow.normal};

  ${media.xl`
    padding: ${Theme.space.xxxl};
  `};
`

export const StepHeader = styled.div`
  position: relative;

  padding: ${Theme.space.xl};
`

export const StepContent = styled.div`
  position: relative;
  flex: 1 1 auto;
  padding: ${Theme.space.xl};
  overflow-y: auto;
`

export const StepFooter = styled.div`
  position: relative;
  display: flex;

  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  margin-top: auto;
  padding: ${Theme.space.xxl};
`
