import * as React from 'react'
import { StepBody, StepContainer, StepContent, StepFooter, StepHeader } from './styles'
import { BackgroundColor } from '../../styles/common'
import { SubTitle, Title } from '../../styles/texts'
import { ContainedButton } from '../../styles/inputs'
import ProgressBar from '../ProgressBar'
import List from '../List'
import StepsStrings from '../../strings/steps'

export interface Props {
  step?: { current?: number; total?: number }
  onNext?: () => void
}

export default function Step({ step, onNext }: Props) {
  const stepStrings = step && step.current ? StepsStrings[step.current] : StepsStrings.default
  const buttonText = step && step.current && step.total ? 
      step.current < step.total ? 'Next' : 'Finish'
    : ''
  return (
    <StepContainer>
      <StepBody>
        <BackgroundColor />
        <StepHeader>
          <Title>{stepStrings.title}</Title>
          <SubTitle>{stepStrings.subtitle}</SubTitle>
        </StepHeader>
        <StepContent>
          {step && <List {...stepStrings.list} current={step.current} />}
        </StepContent>
        <StepFooter>
          <ProgressBar {...step} />
          <ContainedButton onClick={onNext}>
            {buttonText}
          </ContainedButton>
        </StepFooter>
      </StepBody>
    </StepContainer>
  )
}
