import * as React from 'react'
import { ProgressBody, ProgressCaptionBody, ProgressContainer, ProgressFill } from './styles'
import { AccentCaption } from '../../styles/texts'

export interface Props {
  current?: number
  total?: number
  withCaption?: boolean
}

const ProgressBar: React.FC<Props> = ({ current, total, withCaption = true }) => {
  if (!current || !total) return null
  return (
    <ProgressContainer>
      <ProgressBody>
        <ProgressFill percentage={(current / total) * 100} />
      </ProgressBody>
      {withCaption && (
        <ProgressCaptionBody>
          <AccentCaption>{`${current} of ${total} steps`}</AccentCaption>
        </ProgressCaptionBody>
      )}
    </ProgressContainer>
  )
}

export default ProgressBar
