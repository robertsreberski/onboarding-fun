import styled from 'styled-components'
import Theme from '../../theme'

export const ProgressContainer = styled.div`
  position: relative;

  width: 30%;
`

export const ProgressBody = styled.div`
  position: relative;

  width: 100%;
  height: 16px;

  background-color: ${Theme.color.background.grey};

  overflow: hidden;

  border-radius: 8px;
`

export const ProgressFill = styled.div<{ percentage?: number }>`
  position: absolute;
  top: 0;
  left: 0;

  width: ${({ percentage }) => (percentage !== undefined ? percentage : 50)}%;
  height: 100%;

  background-color: ${Theme.color.accent};

  border-radius: 8px;

  transition: width 500ms;
`

export const ProgressCaptionBody = styled.div`
  margin: ${Theme.space.lg} ${Theme.space.md} 0;
`
