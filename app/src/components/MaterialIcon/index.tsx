import * as React from 'react'
import { Iconic } from '../../styles/texts'

export interface Props {
  name: string
}

const MaterialIcon: React.FC<Props> = ({ name }) => {
  return <Iconic className="material-icons">{name}</Iconic>
}

export default MaterialIcon
