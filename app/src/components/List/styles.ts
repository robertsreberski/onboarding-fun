import styled from 'styled-components'
import Theme from '../../theme'

export const ListContainer = styled.div``

export const ListBody = styled.li`
  list-style: none;
`

export const ListItem = styled.ul`
  display: flex;

  flex-direction: row;
  align-items: center;

  padding: ${Theme.space.md};
`

export const Point = styled.span`
  display: flex;

  justify-content: center;
  align-items: center;

  margin-right: ${Theme.space.lg};
`
