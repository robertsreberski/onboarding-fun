import * as React from 'react'
import { ListHeader, ListItemText } from '../../styles/texts'
import { ListBody, ListContainer, ListItem, Point } from './styles'
import MaterialIcon from '../MaterialIcon'

export interface Props {
  current?: number
  header: string
  items: {
    title: string
    text: string
  }[]
}

const List: React.FC<Props> = ({ current, header, items }) => {
  return (
    <ListContainer>
      { current && <ListHeader>{`${current}. ${header}`}</ListHeader>}
      <ListBody>
        {items.map(item => (
          <ListItem>
            <Point>
              <MaterialIcon name="check" />
            </Point>
            <ListItemText>
              <b>{item.title}</b> - {item.text}
            </ListItemText>
          </ListItem>
        ))}
      </ListBody>
    </ListContainer>
  )
}

export default List
