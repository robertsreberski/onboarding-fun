import * as React from 'react'
import { ContainedButton } from '../../styles/inputs'
import { FormBody } from './styles'
import { SimpleInput } from '../../styles/inputs'
import { useCallback, useState } from 'react'

export interface Props {
  name: string | null
  onNameEntered: (name: string) => void
}

const NameForm: React.FC<Props> = ({ name, onNameEntered }) => {
  const [value, setValue] = useState(name || '')

  const onEnterPressed = useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key !== 'Enter' || value.length < 3) {
      return
    }
    onNameEntered(value)
  }, [value, onNameEntered])
  
  const onInputChanged = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const trimmedValue = e.target.value.trim()
    setValue(trimmedValue)
  }, [setValue])

  return (
    <FormBody>
      <SimpleInput
        type="text"
        value={value}
        onChange={onInputChanged}
        onKeyDown={onEnterPressed}
      />
      <ContainedButton disabled={value.length < 3} onClick={() => onNameEntered(value)}>
        Enter
      </ContainedButton>
    </FormBody>
  )
}

export default NameForm
