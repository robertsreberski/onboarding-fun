import styled from 'styled-components'
import Theme from '../../theme'

export const FormBody = styled.div`
  display: flex;

  margin-top: ${Theme.space.xxxl};

  flex-direction: row;
  align-items: center;
`
