import * as React from 'react'
import { FullCenter } from '../../styles/common'
import ReactLoading from 'react-loading'
import Theme from '../../theme'

export interface Props {}

const Loader: React.FC<Props> = ({}) => {
  return (
    <FullCenter>
      <ReactLoading type="spinningBubbles" color={Theme.color.accent} />
    </FullCenter>
  )
}

export default Loader
