import * as React from 'react'
import { Provider } from 'react-redux'
import initStore from './store'
import { Component } from 'react'
import { GlobalStyle } from './styles/global'
import { PersistGate } from 'redux-persist/integration/react'
import Loader from './components/Loader'
import SimpleRouter from './navigation/SimpleRouter'

interface Props {}
interface State {
  resourcesLoaded: boolean
}

const { store, persistor } = initStore()

export default class App extends Component<Props, State> {
  constructor(props: Readonly<Props>) {
    super(props)
  }

  render(): React.ReactNode {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loader />} persistor={persistor}>
          <GlobalStyle />
          <SimpleRouter />
        </PersistGate>
      </Provider>
    )
  }
}
