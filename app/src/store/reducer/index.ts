import { combineReducers } from 'redux'
import GeneralReducer from './reducers/GeneralReducer'
import OnboardingReducer from './reducers/OnboardingReducer'

const appReducer = combineReducers({
  General: GeneralReducer,
  Onboarding: OnboardingReducer,
})

export default appReducer
