export const ACTION_ONBOARDING = {
  REQUEST: {
    START: '@onboarding/request/start',
    FINISH: '@onboarding/request/finish',
    NEXT_STEP: '@onboarding/request/next-step',
  },
  COMMIT: '@onboarding/COMMIT',
  ROLLBACK: '@onboarding/ROLLBACK',
}

export const requestStartOnboarding: () => IReducerAction<undefined> = () => ({
  type: ACTION_ONBOARDING.REQUEST.START,
  payload: undefined,
})

export const requestFinishOnboarding: (id: string) => IReducerAction<IOnboarding> = id => ({
  type: ACTION_ONBOARDING.REQUEST.FINISH,
  payload: {
    _id: id,
    finishedAt: new Date(),
  },
})
export const requestNextStepOfOnboarding: (
  id: string,
  current: number
) => IReducerAction<IOnboarding> = (id, current) => ({
  type: ACTION_ONBOARDING.REQUEST.NEXT_STEP,
  payload: {
    _id: id,
    step: {
      current: current + 1,
    },
  },
})

export const rollbackOnboarding: (id: string) => IReducerAction<IOnboarding> = id => ({
  type: ACTION_ONBOARDING.ROLLBACK,
  payload: {
    _id: id,
  },
})

export const commitOnboarding: (
  onboarding: IOnboarding
) => IReducerAction<IOnboarding> = onboarding => ({
  type: ACTION_ONBOARDING.COMMIT,
  payload: onboarding,
})
