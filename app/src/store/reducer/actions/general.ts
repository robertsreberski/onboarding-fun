export const ACTION_GENERAL = {
  CHANGE_VALUE: '@general/CHANGE_VALUE',
}

export const requestSetUserName: (
  userName: string | null
) => IReducerAction<Partial<IGeneralState>> = userName => ({
  type: ACTION_GENERAL.CHANGE_VALUE,
  payload: {
    activeUserName: userName,
  },
})

export const setActiveOnboarding: (
  id: string | null
) => IReducerAction<Partial<IGeneralState>> = id => ({
  type: ACTION_GENERAL.CHANGE_VALUE,
  payload: {
    activeOnboardingId: id,
  },
})
