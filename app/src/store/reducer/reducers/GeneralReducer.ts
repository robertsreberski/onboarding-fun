import { Reducer } from 'redux'
import { ACTION_GENERAL } from '../actions/general'

const initialState: IGeneralState = {
  activeUserName: null,
  activeOnboardingId: null,
}

const GeneralReducer: Reducer<IGeneralState, IReducerAction<Partial<IGeneralState>>> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case ACTION_GENERAL.CHANGE_VALUE: {
      const { payload } = action

      return {
        ...state,
        ...payload,
      }
    }
    default: {
      return state
    }
  }
}

export default GeneralReducer
