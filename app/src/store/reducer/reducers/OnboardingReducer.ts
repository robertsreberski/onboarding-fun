import { Reducer } from 'redux'
import { ACTION_ONBOARDING } from '../actions/onboarding'

const initialState: IOnboardingState = {}

export const extendPlain = <T>(o: T) => (delta: Partial<T>): T => ({ ...o, ...delta })

const OnboardingReducer: Reducer<IOnboardingState, IReducerAction<IOnboarding>> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case ACTION_ONBOARDING.REQUEST.NEXT_STEP:
    case ACTION_ONBOARDING.REQUEST.FINISH: {
      const { payload } = action
      return extendPlain(state)({
        [payload._id]: {
          ...state[payload._id],
          ...payload,
          step: {
            ...state[payload._id].step,
            ...payload.step,
          },
          _rollback: state[payload._id],
        },
      })
    }
    case ACTION_ONBOARDING.COMMIT: {
      const { payload } = action

      return extendPlain(state)({
        [payload._id]: {
          ...payload,
        },
      })
    }
    case ACTION_ONBOARDING.ROLLBACK: {
      const { payload } = action

      if (state[payload._id]._rollback === undefined) {
        return state
      }

      return extendPlain(state)({
        [payload._id]: {
          ...state[payload._id]._rollback!,
          _rollback: undefined,
        },
      })
    }
    default:
      return state
  }
}

export default OnboardingReducer
