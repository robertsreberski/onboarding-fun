import { createSelector } from 'reselect'
import { getActiveOnboardingId } from './general'

const getOnboardingState = (state: AppState) => state.Onboarding

export const GetActiveOnboarding = createSelector<
  AppState,
  string | null,
  IOnboardingState,
  IOnboarding | null
>(
  getActiveOnboardingId,
  getOnboardingState,
  (onboardingId, onboardings) => {
    if (!onboardingId) return null
    return onboardings[onboardingId]
  }
)
