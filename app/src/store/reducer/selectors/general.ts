export const getActiveOnboardingId = (state: AppState) => state.General.activeOnboardingId
export const getActiveUserName = (state: AppState) => state.General.activeUserName
