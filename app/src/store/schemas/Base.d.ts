interface IReducerAction<T> {
  type: string
  payload: T
}

interface AppState {
  General: IGeneralState
  Onboarding: IOnboardingState
}
