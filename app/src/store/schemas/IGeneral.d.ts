interface IGeneral {
  activeUserName: string | null
  activeOnboardingId: string | null
}

interface IGeneralState extends IGeneral {}
