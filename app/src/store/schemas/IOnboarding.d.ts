interface IOnboarding {
  _id: string
  startedAt?: Date
  finishedAt?: Date | null
  step?: {
    current?: number
    total?: number
  }
  imageUrl?: string
}

interface IOnboardingState {
  [_id: string]: IOnboarding & { _rollback?: IOnboarding }
}
