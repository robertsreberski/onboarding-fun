import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import appReducer from './reducer'
import rootSaga from './sagas'
import initializeApi from './api/initialize'

const persistConfig = {
  key: 'general',
  storage,
  whitelist: ['General'],
}

const persistedReducer = persistReducer(persistConfig, appReducer)

const sagaMiddleware = createSagaMiddleware()

export default function initStore(preloadedState: AppState = {} as AppState) {
  const store = createStore(
    persistedReducer,
    preloadedState,
    compose(
      process.env.NODE_ENV === 'development'
        ? applyMiddleware(sagaMiddleware, logger)
        : applyMiddleware(sagaMiddleware)
    )
  )

  const persistor = persistStore(store, undefined, () => {
    initializeApi(store).then(() => {
      console.log('Api has been initialized!')
    })
  })

  sagaMiddleware.run(rootSaga)

  return { store, persistor }
}
