import { Store } from 'redux'
import { SubscriptionApi } from './subscribers'
import { getSocket } from './index'

export default async function initializeApi(store: Store) {
  await SubscriptionApi.subscribeAll(getSocket(), store)
}
