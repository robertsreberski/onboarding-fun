import { Store } from 'redux'
import { OnboardingRemoteApi } from './remote/onboarding'
import { setActiveOnboarding } from '../reducer/actions/general'
import { commitOnboarding, requestStartOnboarding } from '../reducer/actions/onboarding'
import { GetActiveOnboarding } from '../reducer/selectors/onboarding'
import { getActiveUserName } from '../reducer/selectors/general'
import { getSocket } from './index'
import Socket = SocketIOClient.Socket

export namespace SubscriptionApi {
  type Subscriber = (socket: Socket, store: Store) => Promise<void>
  const subscribeToOnboardings: Subscriber = async (socket: Socket, store: Store) => {
    const activeUserName = getActiveUserName(store.getState())

    if (activeUserName) {
      const activeOnboarding = await OnboardingRemoteApi.getActiveOnboarding(activeUserName)
      if (activeOnboarding) {
        store.dispatch(commitOnboarding(activeOnboarding))
        store.dispatch(setActiveOnboarding(activeOnboarding._id))
      } else {
        store.dispatch(requestStartOnboarding())
      }
    }

    socket.on('onboarding-update', (data: IOnboarding) => {
      const localActive = GetActiveOnboarding(store.getState())
      store.dispatch(commitOnboarding(data))

      if (!data.startedAt) return

      if (!localActive || !localActive.startedAt || localActive.startedAt < data.startedAt) {
        store.dispatch(setActiveOnboarding(data._id))
      }
    })
  }

  export const refreshSubscriptionRoom: (newRoom: string | null) => void = newRoom => {
    if (newRoom) getSocket().emit('subscribe', newRoom)
  }

  export async function subscribeAll(socket: Socket, store: Store) {
    const activeUserName = getActiveUserName(store.getState())
    refreshSubscriptionRoom(activeUserName)

    const subscriptions: Subscriber[] = [
      subscribeToOnboardings,
    ]

    for (const subscribe of subscriptions) {
      await subscribe(socket, store)
    }
  }
}
