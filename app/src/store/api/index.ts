import axios from 'axios'
import * as io from 'socket.io-client'

export const BACKEND_URL = `http://${
  process.env.NODE_ENV === 'development' ? 'localhost' : process.env.SERVER_IP
}:5000/`
const backendInstance = axios.create({
  baseURL: BACKEND_URL,
  timeout: 5000,
})
const socket = io.connect(BACKEND_URL)

const getBackend = () => backendInstance
export const getSocket = () => socket

export default async function executeQuery<T>(
  url: string,
  type: 'post' | 'get' = 'get',
  data = {}
): Promise<T | null> {
  try {
    const backend = getBackend()
    let response: { result: string; data: T | string }

    switch (type) {
      case 'get':
        response = (await backend.get(url)).data
        break
      case 'post':
        response = (await backend.post(url, data)).data
        break
      default:
        response = { result: 'error', data: '' }
    }

    if (response.result === 'success') {
      return response.data as T
    } else if (response.result === 'error') {
      console.log('[Remote] Error:', response.data)
      return null
    }
  } catch (e) {
    console.log('[Axios] Error:', e)
  }

  return null
}
