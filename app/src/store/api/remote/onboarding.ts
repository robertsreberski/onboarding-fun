import executeQuery from '../index'

export namespace OnboardingRemoteApi {
  export const getOnboarding: (id: string) => Promise<IOnboarding | null> = id =>
    executeQuery(`/onboardings/${id}`)

  export const getActiveOnboarding: (userName: string) => Promise<IOnboarding | null> = userName =>
    executeQuery(`/${encodeURIComponent(userName)}/onboardings/current`)

  export const createOnboarding: (userName: string) => Promise<string | null> = userName =>
    executeQuery(`/${encodeURIComponent(userName)}/onboardings`, 'post')

  export const updateOnboarding: (id: string, data: IOnboarding) => Promise<IOnboarding | null> = (
    id,
    data
  ) => executeQuery(`/onboardings/${id}`, 'post', data)
}
