import { all } from '@redux-saga/core/effects'
import onboardingSagas from './onboarding'
import userSagas from './user'

export default function* rootSaga() {
  yield all([...onboardingSagas, ...userSagas])
}
