import {
  ACTION_ONBOARDING,
  requestFinishOnboarding,
  requestNextStepOfOnboarding,
  requestStartOnboarding,
  rollbackOnboarding,
} from '../reducer/actions/onboarding'
import { call, takeLatest, put, delay, select } from '@redux-saga/core/effects'
import { OnboardingRemoteApi } from '../api/remote/onboarding'
import { setActiveOnboarding } from '../reducer/actions/general'
import { getActiveUserName } from '../reducer/selectors/general'

export function* createOnboarding(_: ReturnType<typeof requestStartOnboarding>) {
  const userName = yield select(getActiveUserName)
  if (!userName) return

  yield put(setActiveOnboarding(null))

  const result = yield call(OnboardingRemoteApi.createOnboarding, userName)

  if (!result) {
    yield delay(500)
    yield put(requestStartOnboarding())
    return
  }

  yield put(setActiveOnboarding(result))
}

export function* finishOnboarding({
  payload: { _id, finishedAt },
}: ReturnType<typeof requestFinishOnboarding>) {
  if (!_id) return

  const result = yield call(OnboardingRemoteApi.updateOnboarding, _id, {
    _id,
    finishedAt,
  })

  if (!result) {
    yield put(rollbackOnboarding(_id))
  }
}

export function* nextStepOfOnboarding({
  payload: { _id, step },
}: ReturnType<typeof requestNextStepOfOnboarding>) {
  if (!_id) return

  const result = yield call(OnboardingRemoteApi.updateOnboarding, _id, {
    _id,
    step,
  })

  if (!result) {
    yield put(rollbackOnboarding(_id))
  }
}

const onboardingSagas = [
  takeLatest(ACTION_ONBOARDING.REQUEST.START, createOnboarding),
  takeLatest(ACTION_ONBOARDING.REQUEST.FINISH, finishOnboarding),
  takeLatest(ACTION_ONBOARDING.REQUEST.NEXT_STEP, nextStepOfOnboarding),
]

export default onboardingSagas
