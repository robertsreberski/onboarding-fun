import { takeEvery, put, call } from '@redux-saga/core/effects'
import { ACTION_GENERAL, requestSetUserName, setActiveOnboarding } from '../reducer/actions/general'
import { OnboardingRemoteApi } from '../api/remote/onboarding'
import { commitOnboarding, requestStartOnboarding } from '../reducer/actions/onboarding'
import { SubscriptionApi } from '../api/subscribers'

export function* setActiveUserName({
  payload: { activeUserName },
}: ReturnType<typeof requestSetUserName>) {
  if (activeUserName === undefined) {
    return
  }

  if (!activeUserName) {
    yield put(setActiveOnboarding(null))
    return
  }

  yield call(SubscriptionApi.refreshSubscriptionRoom, activeUserName)
  const activeOnboarding = yield call(OnboardingRemoteApi.getActiveOnboarding, activeUserName)

  if (activeOnboarding) {
    yield put(commitOnboarding(activeOnboarding))
    yield put(setActiveOnboarding(activeOnboarding._id))
  } else {
    yield put(requestStartOnboarding())
  }
}

const userSagas = [takeEvery(ACTION_GENERAL.CHANGE_VALUE, setActiveUserName)]

export default userSagas
