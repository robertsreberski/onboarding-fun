import * as React from 'react'
import { useCallback } from 'react'
import { BackgroundImage } from '../../styles/common'
import Step from '../../components/Step'
import Final from '../../components/Final'
import withLoadable from '../../hoc/withLoadable'
import { noop } from '../../utils'

export type StateProps = {
  onboarding: IOnboarding | null
}

export type DispatchProps = {
  onNextStep: (id: string, next: number) => void
  onFinish: (id: string) => void
  onRestart: () => void
  onLogout: () => void
}

export type OwnProps = {}

type Props = StateProps & DispatchProps & OwnProps

type UseOnboarding = <
  Q extends IOnboarding, 
  R extends (onboardingData: Q) => void, 
  S extends (onboardingData: Q) => void
>(onboardingData: Q, onNextStep: R, onFinish: S) => ({ 
  isStepRecognizable: boolean,
  isOnboardingFinished: boolean,
  onNext: () => void,
})

const useOnboarding: UseOnboarding = (onboardingData, onNextStep, onFinish) => {
  if (!onboardingData || !onboardingData.step) {
    return { isStepRecognizable: false, isOnboardingFinished: false, onNext: noop }
  }
  const { finishedAt, step } = onboardingData
  const { current, total } = step

  const isStepRecognizable = !!current && !!total
  const isOnboardingFinished = !!finishedAt

  if (!isStepRecognizable || isOnboardingFinished) {
    return { isStepRecognizable, isOnboardingFinished, onNext: noop }
  }

  const hasNextStep = current! < total!

  const onNext = useCallback(
    hasNextStep ? () => onNextStep(onboardingData) : () => onFinish(onboardingData)
  , [onboardingData])

  return { isStepRecognizable, isOnboardingFinished, onNext }
}

const Component: React.FC<Props> = ({ onboarding, onNextStep, onFinish, onRestart, onLogout }) => {
  if (!onboarding || !onboarding.step) return null

  const { isStepRecognizable, isOnboardingFinished, onNext } = useOnboarding(
    onboarding, 
    (onboardingData) => onNextStep(onboardingData._id, onboardingData.step!.current!), 
    (onboardingData) => onFinish(onboardingData._id)
  )

  return (
    <>
      <BackgroundImage
        src={onboarding.imageUrl}
        darker={isOnboardingFinished}
      />
      {!isOnboardingFinished && isStepRecognizable && (
        <Step step={onboarding.step} onNext={onNext} />
      )}
      {onboarding.finishedAt && <Final onRestart={onRestart} onLogout={onLogout} />}
    </>
  )
}

export default withLoadable<Props>(({ onboarding }) => !onboarding || !onboarding.step, Component)
