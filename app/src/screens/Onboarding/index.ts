import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import Component, { DispatchProps, StateProps } from './component'
import { GetActiveOnboarding } from '../../store/reducer/selectors/onboarding'
import {
  requestFinishOnboarding,
  requestNextStepOfOnboarding,
  requestStartOnboarding,
} from '../../store/reducer/actions/onboarding'
import { requestSetUserName } from '../../store/reducer/actions/general'

const mapStateToProps: (state: AppState) => StateProps = state => ({
  onboarding: GetActiveOnboarding(state),
})

const mapDispatchToProps: (dispatch: Dispatch) => DispatchProps = dispatch => ({
  onNextStep: (id, current) => {
    dispatch(requestNextStepOfOnboarding(id, current))
  },
  onFinish: (id: string) => {
    dispatch(requestFinishOnboarding(id))
  },
  onRestart: () => {
    dispatch(requestStartOnboarding())
  },
  onLogout: () => {
    dispatch(requestSetUserName(null))
  },
})

const Onboarding = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component)

export default Onboarding
