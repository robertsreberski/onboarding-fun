import * as React from 'react'
import { BackgroundColor, FullCenter } from '../../styles/common'
import { SubTitle, Title } from '../../styles/texts'
import NameForm from '../../components/NameForm'
import NameStrings from '../../strings/name'

export type StateProps = {
  userName: string | null
}

export type DispatchProps = {
  onSetActiveName: (userName: string) => void
}

export type OwnProps = {}

type Props = StateProps & DispatchProps & OwnProps

const Component: React.FC<Props> = ({ userName, onSetActiveName }) => {
  return (
    <>
      <BackgroundColor />
      <FullCenter>
        <Title>{NameStrings.title}</Title>
        <SubTitle>{NameStrings.subtitle}</SubTitle>
        <NameForm name={userName} onNameEntered={onSetActiveName} />
      </FullCenter>
    </>
  )
}

export default Component
