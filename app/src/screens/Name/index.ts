import { connect } from 'react-redux'
import { Dispatch } from 'redux'

import Component, { DispatchProps, StateProps } from './component'
import { requestSetUserName } from '../../store/reducer/actions/general'
import { getActiveUserName } from '../../store/reducer/selectors/general'

const mapStateToProps: (state: AppState) => StateProps = state => ({
  userName: getActiveUserName(state),
})

const mapDispatchToProps: (dispatch: Dispatch) => DispatchProps = dispatch => ({
  onSetActiveName: (userName: string) => {
    dispatch(requestSetUserName(userName))
  },
})

const Name = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component)

export default Name
