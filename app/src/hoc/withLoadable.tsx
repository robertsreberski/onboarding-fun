import * as React from 'react'
import { createElement } from 'react'
import ReactLoading from 'react-loading'
import { FullCenter } from '../styles/common'
import Theme from '../theme'

const withLoadable = <T extends {}>(isLoading: (props: T) => boolean, Component: React.FC<T>) => (
  props: T
) => {
  if (isLoading(props))
    return (
      <FullCenter>
        <ReactLoading type="spinningBubbles" color={Theme.color.accent} />
      </FullCenter>
    )
  return createElement(Component, props)
}

export default withLoadable
