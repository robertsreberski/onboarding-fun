import uuidv4 = require('uuid/v4')
import handle from '../../src/utils/handle'

describe(`util function 'handle'`, () => {
  it('returns success response if no exception happen', async () => {
    const mockedId = uuidv4()
    const callback = async () => {
      return mockedId
    }

    const result = await handle(callback)
    expect(result.result).toBe('success')
    expect(result.data).toBe(mockedId)
  })

  it('returns error message if expection happen', async () => {
    const mockedMessage = uuidv4()
    const callback = async () => {
      throw Error(mockedMessage)
    }

    const result = await handle(callback)
    expect(result.result).toBe('error')
    expect(result.data).toBe(mockedMessage)
  })
})
