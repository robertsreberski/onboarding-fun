export const getErrorMessage: (message: string) => { result: 'error'; data: string } = message => ({
  result: 'error',
  data: message,
})

const handle: <T>(
  action: () => Promise<T>
) => Promise<{ result: 'success' | 'error'; data: T | string }> = async action => {
  try {
    return {
      result: 'success',
      data: await action(),
    }
  } catch (e) {
    return {
      result: 'error',
      data: e.message,
    }
  }
}

export default handle
