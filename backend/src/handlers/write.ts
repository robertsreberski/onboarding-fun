import handle from '../utils/handle'
import axios from 'axios'
import mongoose from 'mongoose'
import { CreateService } from '../service/create'
import { UpdateService } from '../service/update'
import { IOnboardingMongoose } from '../models/IOnboardingMongoose'

export namespace WriteHandlers {
  const getRandomImageUrl: () => Promise<string> = async () => {
    const response = await axios.get('https://source.unsplash.com/user/daunation/1280x720', {})

    return response.request.res.responseUrl as string
  }

  export const createOnboarding: (
    userName: string
  ) => ReturnType<typeof handle> = async userName => {
    return handle(async () => {
      return CreateService.createOnboarding({
        userName: userName,
        startedAt: new Date(),
        step: {
          current: 1,
          total: 3,
        },
        imageUrl: await getRandomImageUrl(),
      })
    })
  }

  export const updateOnboarding: (
    onboardingId: string,
    data: Partial<IOnboardingMongoose>
  ) => ReturnType<typeof handle> = async (onboardingId, data) => {
    return handle(() => {
      const id = mongoose.Types.ObjectId(onboardingId)
      return UpdateService.updateOnboarding(id, {
        ...data,
        ...(data.finishedAt ? { finishedAt: new Date() } : {}),
      })
    })
  }
}
