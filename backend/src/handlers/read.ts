import handle from '../utils/handle'
import mongoose from 'mongoose'
import { ReadService } from '../service/read'

export namespace ReadHandlers {
  export const getActiveOnboarding: (
    userName: string
  ) => ReturnType<typeof handle> = async userName => {
    return handle(async () => {
      return ReadService.getActiveOnboarding(userName)
    })
  }

  export const getOnboarding: (
    onboardingId: string
  ) => ReturnType<typeof handle> = async onboardingId => {
    return handle(async () => {
      const id = mongoose.Types.ObjectId(onboardingId)
      return ReadService.getOnboarding(id)
    })
  }
}
