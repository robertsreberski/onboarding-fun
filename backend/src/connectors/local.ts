import mongoose from 'mongoose'

const DB_NAME = `${process.env.NODE_ENV === 'dev' ? 'dev-' : ''}db-v3`
const DB_URL = `${process.env.NODE_ENV === 'dev' ? 'localhost' : 'mongo'}:27017/${DB_NAME}`

export default () => {
  mongoose
    .connect(`mongodb://${DB_URL}`, {
      useNewUrlParser: true,
      connectTimeoutMS: 60000,
    })
    .then(() => {
      console.log('Mongoose connection established successfully')
    })
    .catch(err => {
      console.log(`Mongoose connection end up with error: ${err}`)
    })
}
