import express from 'express'
import cors from 'cors'
import http from 'http'
import socketio from 'socket.io'
import { getRestInitializers, getSocketGlobalInitializers, getSocketInitializers } from './api'
import initLocalDB from './connectors/local'

const PORT_NUMBER = 5000

initLocalDB()

const rest = express()
rest.use(express.json())
rest.use(
  cors({
    origin: `http://${process.env.NODE_ENV === 'dev' ? 'localhost:8080' : process.env.SERVER_IP}`,
  })
)
const server = new http.Server(rest)
const io = socketio(server)

const restInitializers = getRestInitializers()
const socketInitializers = getSocketInitializers()
const socketGlobalInitializers = getSocketGlobalInitializers()

restInitializers.forEach(init => init(rest))
socketGlobalInitializers.forEach(init => init(io))

io.on('connection', socket => {
  socketInitializers.forEach(init => init(socket))
})

server.listen(PORT_NUMBER, () => {
  console.log(`Onboarding Fun Backend started on port ${PORT_NUMBER}`)
})
