import { Express } from 'express'
import { WriteHandlers } from '../../handlers/write'

export function rest(rest: Express) {
  rest.post(`/:userName/onboardings`, async (req, res) => {
    const { userName } = req.params

    if (!userName) {
      res.status(400).send("'userName' cannot be empty!")
      return
    }

    const onboardingId = await WriteHandlers.createOnboarding(userName)

    res.send(onboardingId)
  })
  rest.post(`/onboardings/:onboardingId`, async (req, res) => {
    const { onboardingId } = req.params
    const onboardingData = req.body

    if (!onboardingId || !onboardingData) {
      res.status(400).send("'onboardingId' and request body cannot be empty!")
    }

    const data = await WriteHandlers.updateOnboarding(onboardingId, onboardingData)

    res.send(data)
  })
}
