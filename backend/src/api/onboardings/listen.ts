import { Socket } from 'socket.io'

export function socket(socket: Socket) {
  socket.on('subscribe', (userName: string) => {
    socket.leaveAll() // not documented well but existing: https://github.com/socketio/socket.io/blob/1decae341c80c0417b32d3124ca30c005240b48a/lib/socket.js#L287
    socket.join(userName)
  })
}
