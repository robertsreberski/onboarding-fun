import { Server } from 'socket.io'
import getEmitter from '../../events/emitter'
import { IOnboardingMongoose } from '../../models/IOnboardingMongoose'

export function socketGlobal(io: Server) {
  getEmitter().on('onboarding-update', (onboarding: IOnboardingMongoose) => {
    io.to(onboarding.userName).emit('onboarding-update', onboarding)
  })
}
