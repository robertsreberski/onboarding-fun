import { Express } from 'express'
import { ReadHandlers } from '../../handlers/read'
import { getErrorMessage } from '../../utils/handle'

export function rest(rest: Express) {
  rest.get(`/:userName/onboardings/current`, async (req, res) => {
    const { userName } = req.params

    if (!userName) {
      res.status(400).send(getErrorMessage("'userName' cannot be empty!"))
      return
    }

    const onboarding = await ReadHandlers.getActiveOnboarding(userName)

    res.send(onboarding)
  })
  rest.get(`/onboardings/:onboardingId`, async (req, res) => {
    const { onboardingId } = req.params

    if (!onboardingId) {
      res.status(400).send(getErrorMessage("'onboardingId' cannot be empty!"))
      return
    }

    const onboarding = await ReadHandlers.getOnboarding(onboardingId)

    res.send(onboarding)
  })
}
