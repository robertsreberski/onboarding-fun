import { Express } from 'express'
import { Server, Socket } from 'socket.io'
import * as fs from 'fs'

const getAllFilesWithAPI: () => {
  rest?: (rest: Express) => void
  socket?: (socket: Socket) => void
  socketGlobal?: (io: Server) => void
}[] = () => {
  const directories = fs
    .readdirSync(__dirname)
    .filter(file => fs.lstatSync(`${__dirname}/${file}`).isDirectory())

  return directories
    .map(dir =>
      fs
        .readdirSync(`${__dirname}/${dir}`)
        .map(file => `${dir}/${file}`)
        .filter(file => !fs.lstatSync(`${__dirname}/${file}`).isDirectory())
    )
    .reduce((acc, curr) => [...acc, ...curr], [])
    .map(apiFile => require(`${__dirname}/${apiFile}`))
}

const AllFilesWithAPI = getAllFilesWithAPI()

export const getRestInitializers = () => {
  return AllFilesWithAPI.filter(api => typeof api.rest === 'function').map(
    api => api.rest as (rest: Express) => void
  )
}

export const getSocketInitializers = () => {
  return AllFilesWithAPI.filter(api => typeof api.socket === 'function').map(
    api => api.socket as (socket: Socket) => void
  )
}

export const getSocketGlobalInitializers = () => {
  return AllFilesWithAPI.filter(api => typeof api.socketGlobal === 'function').map(
    api => api.socketGlobal as (io: Server) => void
  )
}
