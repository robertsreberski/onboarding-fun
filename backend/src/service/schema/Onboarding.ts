import { Model, Schema } from 'mongoose'
import getEmitter from '../../events/emitter'
import * as mongoose from 'mongoose'
import { IOnboardingMongoose } from '../../models/IOnboardingMongoose'

const OnboardingSchema: Schema = new Schema<IOnboardingMongoose>({
  userName: { type: String, required: true },
  startedAt: { type: Date, required: true },
  finishedAt: { type: Date, required: false },
  step: {
    current: { type: Number, required: true },
    total: { type: Number, required: true },
  },
  imageUrl: { type: String, required: true },
})

OnboardingSchema.post('save', (doc: IOnboardingMongoose) => {
  getEmitter().emit('onboarding-update', doc)
})

const Onboarding: Model<IOnboardingMongoose> = mongoose.model<IOnboardingMongoose>(
  'Onboarding',
  OnboardingSchema
)

export default Onboarding
