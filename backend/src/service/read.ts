import * as mongoose from 'mongoose'
import Onboarding from './schema/Onboarding'
import { IOnboardingMongoose } from '../models/IOnboardingMongoose'
import { Document, Model } from 'mongoose'

export namespace ReadService {
  const get: <A extends Document, K extends Model<A>>(
    T: K,
    id: mongoose.Types.ObjectId
  ) => Promise<A> = async (T, id) => {
    const existing = await T.findById(id)
    if (!existing) {
      console.log(`[${T.modelName}] Not exist, create it first.`)
      throw new Error(`${T.modelName} not exist`)
    }

    return existing
  }

  export const getOnboarding: (
    id: mongoose.Types.ObjectId
  ) => Promise<IOnboardingMongoose> = async id => get(Onboarding, id)

  export const getActiveOnboarding: (
    userName: string
  ) => Promise<IOnboardingMongoose> = async userName => {
    const active = await Onboarding.find({ userName })
      .sort({ startedAt: -1 })
      .limit(1)
      .exec()

    if (!active.length) {
      throw new Error('No active Onboardings found')
    }

    return active[0]
  }
}
