import * as mongoose from 'mongoose'
import flatten from 'flat'
import Onboarding from './schema/Onboarding'
import { IOnboardingMongoose } from '../models/IOnboardingMongoose'
import { Document, Model } from 'mongoose'

export namespace UpdateService {
  const update: <A extends Document, K extends Model<A>>(
    T: K,
    id: mongoose.Types.ObjectId,
    data: Partial<A>
  ) => Promise<mongoose.Types.ObjectId> = async (T, id, data) => {
    const existing = await T.findOneAndUpdate({ _id: id }, { $set: flatten(data) }, { new: true })

    if (!existing) {
      console.log(`[${T.modelName}] Not exist, create it first.`)
      throw new Error('Object not exist')
    }

    existing.save()
    console.log(`[${T.modelName}] Has been updated.`)
    return existing._id
  }

  export const updateOnboarding = (
    id: mongoose.Types.ObjectId,
    data: Partial<IOnboardingMongoose>
  ) => update(Onboarding, id, data)
}
