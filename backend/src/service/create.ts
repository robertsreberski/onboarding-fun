import * as mongoose from 'mongoose'
import Onboarding from './schema/Onboarding'
import { IOnboardingMongoose } from '../models/IOnboardingMongoose'
import { Document, Model } from 'mongoose'

export namespace CreateService {
  const create: <A extends Document, K extends Model<A>>(
    T: K,
    data: Partial<A>
  ) => Promise<mongoose.Types.ObjectId> = async (T, data) => {
    const obj = await T.create(data)
    return obj._id
  }

  export const createOnboarding = (data: Partial<IOnboardingMongoose>) => create(Onboarding, data)
}
