import EventEmitter from 'events'

let _emitter: EventEmitter

export default function getEmitter(): EventEmitter {
  if (!_emitter) _emitter = new EventEmitter()
  return _emitter
}
