import { Document } from 'mongoose'

export interface IOnboardingMongoose extends Document {
  userName: string
  startedAt: Date
  finishedAt: Date | null
  step: {
    current: number
    total: number
  }
  imageUrl: string
}
