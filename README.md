App is deployed on http://34.90.212.33/
It is server hosted by Google Cloud.

I haven't enabled HTTPS protocol because that would include attaching domain name and it's more like a Dev-Ops thing.
I know how to do it though: I would need to create certbot docker container to generate Let's Encrypt certificates regularly and nginx server to provide reverse proxy to both app and backend endpoints.

The preview of my initial design can be found here: https://preview.uxpin.com/4abd121d77789b4e0f826a091ee516fa44327023#/pages/112501029